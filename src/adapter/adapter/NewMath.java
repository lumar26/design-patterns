package adapter.adapter;

import adapter.adaptee.Math;
import adapter.target.TargetMath;
import adapter.util.MyInt;

public class NewMath implements TargetMath {

    private final Math adaptee;

    public NewMath(Math adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void plus(MyInt res, int a, int b) {
        res.setNumber(adaptee.plus(a, b));
    }
}
