package adapter.main;

import adapter.adaptee.Math;
import adapter.adaptee.impl.MathImpl;
import adapter.adapter.NewMath;
import adapter.target.TargetMath;
import adapter.util.MyInt;

public class Main {
    public static void main(String[] args) {
        Math math = new MathImpl();
        int res = math.plus(3, 7);
        System.out.println("Rezultat prve operacije je : " + res) ;

//        plus(res, a, b);

        MyInt res2 = new MyInt();
        TargetMath tm = new NewMath(math);
        tm.plus(res2, 4, 5);
        System.out.println("Drugi rezultat je: " + res2.getNumber());
    }
}
