package adapter.adaptee;

public interface Math {
    int plus(int a, int b);
}
