package adapter.adaptee.impl;

import adapter.adaptee.Math;

public class MathImpl implements Math {
    @Override
    public int plus(int a, int b) {
        return a + b;
    }
}
