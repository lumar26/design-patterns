package adapter.util;

public class MyInt {
    private int number;

    public MyInt() {
    }
    public MyInt(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
