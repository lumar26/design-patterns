package client;

import command.controller.Controller;

public class Main {
    public static void main(String[] args) {
        Controller.getInstance().startApp();
    }
}
