package command.view.table_model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.domain.Student;

public class StudentsTableModel extends AbstractTableModel{
    
    List<Student> students = new ArrayList<>();
    String[] columnNames = new String[] {"Name", "Surname", "Student ID"};
    

    @Override
    public int getRowCount() {
        return students.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Student s = students.get(row);
        switch (col) {
            case 0: return s.getName();
            case 1: return s.getSurname();
            case 2: return s.getStudentId();
            default: return "N/A";
        }
    }
    
    public void addStudent(Student s){
        students.add(s);
        fireTableDataChanged();
    }
    
    public void removeStudent(Student s){
        students.remove(s);
        System.out.println("Student removed: " + s);
        fireTableDataChanged();
    }
    
}
