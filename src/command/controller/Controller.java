package command.controller;

import command.model.command.ICommand;
import command.model.command.Invoker;
import command.model.command.impl.AddStudentCommand;
import command.view.StudentForm;
import command.view.table_model.StudentsTableModel;
import model.domain.Student;
import view.AddStudentDialog;

public class Controller { //klijent

    private static Controller instance;
    private StudentForm form;
    private AddStudentDialog dialog;
    private final Invoker tableActionInvoker;

    private Controller() {
        this.tableActionInvoker = new Invoker();
        form = new StudentForm();
        setListeners();
    }

    public void startApp() {
        form.setVisible(true);
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    private void setListeners() {
        form.getMiAddNewPerson().addActionListener(action -> openAddStudentDialog());
        form.getBtnUndo().addActionListener(action -> undoOperation());
        form.getBtnRedo().addActionListener(action -> redoOperation());
    }

    private void redoOperation() {
        tableActionInvoker.redo();
    }

    private void undoOperation() {
        tableActionInvoker.undo();
    }

    private void openAddStudentDialog() {
        dialog = new AddStudentDialog(form, true);
        dialog.getBtnAdd().addActionListener(action -> addStudent());
        dialog.setVisible(true);
    }

    private void addStudent() {
        Student student = new Student(dialog.getTxtName().getText(), dialog.getTxtSurname().getText(), dialog.getTxtStudentId().getText());
        System.out.println("Dodavanje studenta: " + student);
        ICommand addCommand = new AddStudentCommand((StudentsTableModel) form.getTblPersons().getModel(), student);
        tableActionInvoker.setCommand(addCommand);
        tableActionInvoker.execute();
        dialog.dispose();
    }

}
