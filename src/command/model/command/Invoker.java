package command.model.command;

import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<ICommand> commandsList;

    private int currentCommand;

    public Invoker() {
        this.commandsList = new ArrayList<>();
        currentCommand = -1;
    }

    public void setCommand(ICommand command){
        commandsList.add(command);
        currentCommand = commandsList.size() - 1;
    }

    public void execute(){
        commandsList.get(currentCommand).execute();
    }

    public void undo(){
        if (currentCommand > -1){
            commandsList.get(currentCommand).undo();
            currentCommand--;
        }
    }

    public void redo() {
        if (currentCommand < commandsList.size() - 1){
            currentCommand++;
            commandsList.get(currentCommand).execute();
        }
    }
}
