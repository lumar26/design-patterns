package command.model.command;

public interface ICommand {
    void execute();
    void undo();
}
