package command.model.command.impl;

import command.model.command.ICommand;
import command.view.table_model.StudentsTableModel;
import model.domain.Student;

public class AddStudentCommand implements ICommand {
    
    private final StudentsTableModel receiver; // model tabele je u stvari onaj ko izvrsava logiku 
    private final Student student;

    public AddStudentCommand(StudentsTableModel receiver, Student s) { // ovaj konstruktor poziva klijent
        this.receiver = receiver;
        this.student = s;
    }
    
    @Override
    public void execute() {
        receiver.addStudent(student);
    }

    @Override
    public void undo() {
        receiver.removeStudent(student);
    }

    
}
