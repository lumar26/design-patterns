package model.domain;

public class Student {
    private String name;
    private String surname;
    private String studentId;

    public Student(String name, String surname, String studentId) {
        this.name = name;
        this.surname = surname;
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getStudentId() {
        return studentId;
    }

    @Override
    public String toString() {
        return name+"_"+surname+"_"+studentId; 
    }
    
    
}
