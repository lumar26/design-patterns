package observer.view;

import javax.swing.*;

public class MainForm extends javax.swing.JFrame {

    public MainForm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        menuPerson = new javax.swing.JMenu();
        itemAdd = new javax.swing.JMenuItem();
        itemShowInTable = new javax.swing.JMenuItem();
        itemShowInComboBox = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        menuPerson.setText("Person");

        itemAdd.setText("Add");
        menuPerson.add(itemAdd);

        itemShowInTable.setText("Show in table");
        menuPerson.add(itemShowInTable);

        itemShowInComboBox.setText("Show in combo box");
        menuPerson.add(itemShowInComboBox);

        jMenuBar1.add(menuPerson);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 816, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 365, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JMenuItem getItemAdd() {
        return itemAdd;
    }

    public JMenuItem getItemShowInComboBox() {
        return itemShowInComboBox;
    }

    public JMenuItem getItemShowInTable() {
        return itemShowInTable;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem itemAdd;
    private javax.swing.JMenuItem itemShowInComboBox;
    private javax.swing.JMenuItem itemShowInTable;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu menuPerson;
    // End of variables declaration//GEN-END:variables
}
