package observer.view.table_model;

import observer.model.domain.Person;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class PersonTableModel extends AbstractTableModel {

    private List<Person> persons = new ArrayList<>();
    private String[] colNames = new String[] {"First name", "Last name"};

    @Override
    public int getRowCount() {
        return persons.size();
    }

    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Person p = persons.get(row);
        switch (col){
            case 0: return p.getName();
            case 1: return p.getSurname();
            default: return "N/A";
        }
    }

    public void setPersons(List<Person> persons){
        this.persons = persons;
        fireTableDataChanged();
    }


}
