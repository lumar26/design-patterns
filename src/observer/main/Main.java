package observer.main;

import observer.controller.MainFormController;

public class Main {
    public static void main(String[] args) {
        MainFormController.getInstance().launch();
    }
}
