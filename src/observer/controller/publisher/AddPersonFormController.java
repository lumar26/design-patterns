package observer.controller.publisher;

import observer.controller.MainFormController;
import observer.controller.subscriber.ComboViewController;
import observer.controller.subscriber.Subscriber;
import observer.controller.subscriber.TableViewController;
import observer.model.domain.Person;
import observer.view.AddPersonDialog;

import java.util.ArrayList;
import java.util.List;

public class AddPersonFormController {
    private static AddPersonFormController instance;

    private AddPersonDialog form;

    private List<Subscriber> subscribers = new ArrayList<>();

    private AddPersonFormController() {
        form = new AddPersonDialog(MainFormController.getInstance().getForm(), false);
        subscribers.add(TableViewController.getInstance());
        subscribers.add(ComboViewController.getInstance());
    }

    public static AddPersonFormController getInstance() {
        if (instance == null) instance = new AddPersonFormController();
        return instance;
    }

    public void show() {
        form.setVisible(true);
    }


    public void launch() {
        setListeners();
        form.setVisible(true);
    }

    private void setListeners() {
        form.getBtnAdd().addActionListener(a -> addPerson());
    }

    public void subscribe(Subscriber subscriber) {
        if (!subscribers.contains(subscriber))
            subscribers.add(subscriber);
    }

    public void notifySubscribers(){
        for (Subscriber subscriber : subscribers) {
            subscriber.update(MainFormController.getInstance().getPersons());
        }
    }

    private void addPerson() {
        String firstName = form.getTxtFirstName().getText();
        String lastName = form.getTxtLastName().getText();

        MainFormController.getInstance().getPersons().add(new Person(firstName, lastName));
        notifySubscribers();
    }
}
