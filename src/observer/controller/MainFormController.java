package observer.controller;

import observer.controller.publisher.AddPersonFormController;
import observer.controller.subscriber.ComboViewController;
import observer.controller.subscriber.TableViewController;
import observer.model.domain.Person;
import observer.view.MainForm;

import java.util.ArrayList;
import java.util.List;

public class MainFormController {
    private static MainFormController instance;

    private final  MainForm form;
    private final List<Person> persons;

    private MainFormController(){
        form = new MainForm();
        persons = new ArrayList<>();
    }

    public MainForm getForm() {
        return form;
    }

    public void launch(){
        setListeners();
        form.setVisible(true);
    }

    public List<Person> getPersons() {
        return persons;
    }

    private void setListeners() {
        form.getItemAdd().addActionListener(a -> AddPersonFormController.getInstance().launch());
        form.getItemShowInComboBox().addActionListener(a -> ComboViewController.getInstance().show());
        form.getItemShowInTable().addActionListener(a -> TableViewController.getInstance().show());
    }

    public static MainFormController getInstance() {
        if (instance == null) instance = new MainFormController();
        return instance;
    }
}
