package observer.controller.subscriber;

import observer.controller.MainFormController;
import observer.model.domain.Person;
import observer.view.ShowPersonTable;
import observer.view.table_model.PersonTableModel;

import java.util.ArrayList;
import java.util.List;

public class TableViewController implements Subscriber{
    private static TableViewController instance;

    private List<ShowPersonTable> forms;
    
    private  TableViewController() {
        forms = new ArrayList<>();
    }
    
    public void show(){
        update(MainFormController.getInstance().getPersons());

        forms.add(new ShowPersonTable(MainFormController.getInstance().getForm(), false));
        forms.get(forms.size() - 1).setVisible(true);
    }


    public static TableViewController getInstance() {
        if (instance == null) instance = new TableViewController();
        return instance;
    }

    @Override
    public void update(List<Person> persons) {
        for (ShowPersonTable f : forms){
            ((PersonTableModel) f.getTblPersons().getModel()).setPersons(persons);
        }
    }
}
