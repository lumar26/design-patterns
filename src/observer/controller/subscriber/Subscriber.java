package observer.controller.subscriber;

import observer.model.domain.Person;

import java.util.List;

public interface Subscriber {
    void update(List<Person> persons);
}
