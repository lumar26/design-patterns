package observer.controller.subscriber;

import observer.controller.MainFormController;
import observer.model.domain.Person;
import observer.view.ShowPersonComboBox;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class ComboViewController implements Subscriber{
    private static ComboViewController instance;

    private List<ShowPersonComboBox> forms;

    private ComboViewController() {
        forms = new ArrayList<>();
    }

    public static ComboViewController getInstance() {
        if (instance == null) instance = new ComboViewController();
        return instance;
    }

    public void show() {
        update(MainFormController.getInstance().getPersons());
        forms.add(new ShowPersonComboBox(MainFormController.getInstance().getForm(), false));
        forms.get(forms.size() - 1).setVisible(true);
    }

    @Override
    public void update(List<Person> persons) {
        for (ShowPersonComboBox f : forms){
            f.getCmbPersons().setModel(new DefaultComboBoxModel<>(persons.toArray(Person[]::new)));
        }
    }
}
