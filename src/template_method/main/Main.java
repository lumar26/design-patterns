package template_method.main;

import template_method.domain.City;
import template_method.so.AbstractSO;
import template_method.so.impl.SaveCity;

public class Main {

    public static void main(String[] args) {
        AbstractSO saveCity = new SaveCity(new City("Paracin", 26000));
        saveCity.execute();
    }
}
