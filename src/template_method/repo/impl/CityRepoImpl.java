package template_method.repo.impl;

import template_method.domain.City;
import template_method.repo.CityRespository;

public class CityRepoImpl implements CityRespository {
    @Override
    public void save(City city) {
        System.out.println("Saved city: " + city);
    }

    @Override
    public void update(City city) {
        System.out.println("Updated city: " + city);

    }
}
