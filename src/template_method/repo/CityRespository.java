package template_method.repo;

import template_method.domain.City;

public interface CityRespository {
    void save(City city);
    void update(City city);
}
