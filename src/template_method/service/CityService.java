package template_method.service;

import template_method.domain.City;

public interface CityService {
    void save(City city);
    void update(City city);
}
