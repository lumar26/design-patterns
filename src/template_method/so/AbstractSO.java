package template_method.so;

public abstract class AbstractSO {
    public void execute(){
//        make connection
        makeConnection();
//        open transaction
        openTransaction();
//        check precondition

//        execute operation
        if (checkPrecondition()) {
            operation();
            commitTransaction();
        }
        else rollbackTransaction();
//        commit / rollback
//        close connection
        closeConnection();
    }

    private void closeConnection() {
        System.out.println("Konekcija zatvorena");
    }
    private void rollbackTransaction() {
        System.out.println("Ponistavanje transakcije");
    }

    private void commitTransaction() {
        System.out.println("Potvrdjivanje transakcije");
    }

    protected abstract void operation();

    protected abstract boolean checkPrecondition();

    private void openTransaction() {
        System.out.println("Otvaranje transakcije");
    }

    private void makeConnection() {
        System.out.println("Pravljenje konekcije sa bazom");
    }
}
