package template_method.so.impl;

import template_method.domain.City;
import template_method.so.AbstractSO;

public class SaveCity extends AbstractSO {

    private City cityToSave;

    public SaveCity(City cityToSave) {
        this.cityToSave = cityToSave;
    }

    @Override
    protected void operation() {
        System.out.println("Sacuvan je grad: " + cityToSave);
    }

    @Override
    public boolean checkPrecondition() {
        return cityToSave.getPopulation() >= 100;
    }
}
