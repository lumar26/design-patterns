package template_method.so.impl;

import template_method.domain.City;
import template_method.so.AbstractSO;

public class UpdateCity extends AbstractSO {

    private City cityToUpdate;

    public UpdateCity(City cityToUpdate) {
        this.cityToUpdate = cityToUpdate;
    }

    @Override
    protected void operation() {
        System.out.println("Azuriran grad: " + cityToUpdate);
    }

    @Override
    protected boolean checkPrecondition() {
        return !cityToUpdate.getName().isBlank();
    }
}
