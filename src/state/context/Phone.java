package state.context;

import state.state.State;

public class Phone {
    private String name;
    private State state;

    public Phone(String name) {
        this.name = name;
    }

    public void lockButtonPressed(){
        state.pressLockButton();
    }

    public void setStatus(State status){
        this.state = status;
    }
}
