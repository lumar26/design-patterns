package state.state.impl;

import state.context.Phone;
import state.state.State;

public class Locked extends State {

    public Locked(Phone context) {
        super(context);
    }

    @Override
    public void pressLockButton() {
        System.out.println("Pressed lock button while phone is locked...");
        State nextStatus = new Unlocked(this.context);
        context.setStatus(nextStatus);
        System.out.println("Changed to state: " + nextStatus);
    }

    @Override
    public String toString() {
        return "Locked";
    }
}
