package state.state.impl;

import state.context.Phone;
import state.state.State;

public class RingingUnlocked extends Ringing{

    public RingingUnlocked(Phone context) {
        super(context);
    }

    @Override
    protected State changeStatus(Phone context) {
        State newState = new Unlocked(this.context);
        context.setStatus(newState);
        return newState;
    }
}
