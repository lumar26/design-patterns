package state.state.impl;

import state.context.Phone;
import state.state.State;

public abstract class Ringing extends State {

    public Ringing(Phone context) {
        super(context);
    }

    @Override
    public void pressLockButton() {
        System.out.println("Pressed lock button while phone is ringing...");
        State nextStatus = changeStatus(context);
        System.out.println("Changed to state: " + nextStatus);
    }

    protected abstract State changeStatus(Phone context);

    @Override
    public String toString() {
        return "Ringing";
    }
}
