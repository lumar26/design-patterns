package state.state.impl;

import state.context.Phone;
import state.state.State;

public class RingingLocked extends Ringing{
    public RingingLocked(Phone context) {
        super(context);
    }

    @Override
    protected State changeStatus(Phone context) {
        State newState = new Locked(this.context);
        context.setStatus(newState);
        return newState;
    }
}
