package state.state.impl;

import state.context.Phone;
import state.state.State;

public class Unlocked extends State {

    public Unlocked(Phone context) {
        super(context);
    }

    @Override
    public void pressLockButton() {
        System.out.println("Pressed lock button while phone is unlocked...");
        State nextStatus = new Locked(this.context);
        context.setStatus(nextStatus);
        System.out.println("Changed to state: " + nextStatus);
    }

    @Override
    public String toString() {
        return "Unlocked";
    }
}
