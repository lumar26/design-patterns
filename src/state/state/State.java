package state.state;

import state.context.Phone;

public abstract class State {
    protected Phone context;

    public State(Phone context) {
        this.context = context;
    }

    public abstract void pressLockButton();
}
