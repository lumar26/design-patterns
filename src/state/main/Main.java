package state.main;

import state.context.Phone;
import state.state.State;
import state.state.impl.Locked;
import state.state.impl.RingingLocked;

public class Main {
    public static void main(String[] args) {
        Phone phone = new Phone("iPhone");
        State initialState = new Locked(phone);
        phone.setStatus(initialState);
        phone.lockButtonPressed();
        phone.lockButtonPressed();
        phone.setStatus(new RingingLocked(phone));
        phone.lockButtonPressed();
    }
}
